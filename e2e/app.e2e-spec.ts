import { DvoramaPage } from './app.po';

describe('dvorama App', function() {
  let page: DvoramaPage;

  beforeEach(() => {
    page = new DvoramaPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
