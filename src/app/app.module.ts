import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {UsersService } from './users/users.service';
import { RouterModule, Routes } from '@angular/router';
import{AngularFireModule} from 'angularFire2';

import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { UserComponent } from './user/user.component';
import { SpinnerComponent } from './shared/spinner/spinner.component';
import { PostsComponent } from './posts/posts.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { UserFormComponent } from './user-form/user-form.component';
export const firebaseconfig ={
   apiKey: "AIzaSyA_vncdwALczQmA9wMicrOTTQl2jI0_Ws4",
    authDomain: "mevozarot-2d0ee.firebaseapp.com",
    databaseURL: "https://mevozarot-2d0ee.firebaseio.com",
    storageBucket: "mevozarot-2d0ee.appspot.com",
    messagingSenderId: "389025766453"
}
const appRoutes:Routes = [
 
  {path:'users',component:UsersComponent},
  {path:'posts',component:PostsComponent},
  {path:'',component:UsersComponent},//כשלא מצוין ראוט הדיפולטיבי הוא יוזר
 {path:'**',component:PageNotFoundComponent},//כשיש עמוד לא קיים
]



@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    UserComponent,
    SpinnerComponent,
    PostsComponent,
    PageNotFoundComponent,
    UserFormComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule,
RouterModule.forRoot(appRoutes),
  AngularFireModule.initializeApp(firebaseconfig) 
  ],
  providers: [UsersService],
  bootstrap: [AppComponent]
})
export class AppModule { }
