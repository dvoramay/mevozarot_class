
import { Component, OnInit } from '@angular/core';
import{UsersService} from './users.service'
@Component({
  selector: 'jce-users',
  templateUrl: './users.component.html',
  styles: [`
    .users li { cursor: default; }
    .users li:hover { background: #ecf0f1; }
    .list-group-item.active, 
    .list-group-item.active:hover { 
         background-color: #ecf0f1;
         border-color: #ecf0f1; 
         color: #2c3e50;
    }     
  `]
})
export class UsersComponent implements OnInit {
isLoading:Boolean = true;
 users;
currrentUser;
select(user){
  this.currrentUser =user;

}
  constructor(private _userService:UsersService) { }

addUser(user){
  this._userService.addUser(user);
}


deleteUser(user){
  this.users.splice(
    this.users.indexOf(user),1
  ) 
}
 editUser(originalAndEdited){
    this.users.splice(
      this.users.indexOf(originalAndEdited[0]),1,originalAndEdited[1]  
    )
    console.log(this.users);
  }  

  
  ngOnInit() {
   this._userService.getusers().subscribe(usersData=>
   {this.users=usersData;
     this.isLoading=false;
    console.log(this.users)  });

  }

}