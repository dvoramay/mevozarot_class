import { Component } from '@angular/core';
import {AngularFire} from 'angularFire2';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app really works!';
  constructor(af:AngularFire){
    console.log(af);
  };
}
